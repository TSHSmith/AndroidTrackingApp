package tshs.trackingapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

import tshs.trackingapp.Controllers.*;
import tshs.trackingapp.Services.ServiceManager;
import tshs.trackingapp.Statics.*;
import tshs.trackingapp.Threads.APICom;

public class MainActivity extends Activity {

    private Button btnShowloc;
    private ThreadController tC;

    private FileController fK = new FileController();
    private String key;

    private Intent i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(StaticValues.messageList == null)
            StaticValues.messageList = new ArrayList<>();

        btnShowloc = (Button) findViewById(R.id.btnGO);

        if(StaticValues.hasPressed){
            btnShowloc.setText("Stop Tracking");
        }

        i = new Intent(this, ServiceManager.class);

        new getMessage().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);



        btnShowloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThreadButton();
            }
        });



        this.manageKey();
        LocalDatabase.setDatabase(openOrCreateDatabase("APIurl", MODE_PRIVATE, null));

        return true;
    }

    private void manageKey(){
        this.key = fK.ReadKey(this);
        if(key == "") {
            this.KeyInput();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void ThreadButton(){
        tC = new ThreadController(MainActivity.this, key);

        if(!StaticValues.hasPressed) {
            StaticValues.hasPressed = true;
            btnShowloc.setText("Stop Tracking");
            if(tC.ApiRunning())
                StaticValues.continueAPI = true;
            else
                tC.StartApi();


            i.putExtra("KEY", key);
            startService(i);

        } else {
            StopPress(false);
        }
    }


    private void StopPress(boolean delete){
        btnShowloc.setText("Start Tracking");
        tC.StopApi();
        stopService(i);
        if(delete)
            LocalDatabase.execSQL("DELETE FROM urlTable");

        StaticValues.hasPressed = false;
    }


    private void KeyInput(){

        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.details, null);

        final EditText codeInput = (EditText) textEntryView.findViewById(R.id.codeInput);
        final EditText nameInput = (EditText) textEntryView.findViewById(R.id.nameInput);
        final Spinner trackable = (Spinner) textEntryView.findViewById(R.id.trackableSpinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.trackable_array, android.R.layout.simple_spinner_dropdown_item);
        trackable.setAdapter(adapter);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setIcon(R.drawable.cast_ic_notification_0).setTitle("Enter device info:").setView(textEntryView);


        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CheckKey(codeInput.getText().toString(), nameInput.getText().toString(), trackable.getSelectedItem().toString());
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    public void CheckKey(String key, String name, String trackable){
        Pattern p = Pattern.compile("[^a-zA-Z0-9\" \"]");
        boolean hasSpecialChar = p.matcher(name).find();

        if(!hasSpecialChar) {
            try {
                String url = "http://andtrackproject.azurewebsites.net/API/APP_REG?regkey=" + key + "&dn=" + name + "&tb=" + trackable;
                url = url.replace(" ", "%20");
                String json = new APICom().startThread(url);
                JSONObject jsonObject = new JSONObject(json);
                if (jsonObject.getBoolean("success")) {
                    //Check if key is legit and save to a textfile.
                    String newKey = jsonObject.getString("deviceID");
                    fK.WriteKey(newKey, this);
                    this.key = newKey;
                    showToast("Device registered");
                } else {
                    //get error message and title from the json
                    showToast(jsonObject.getString("message"));
                    this.KeyInput();
                }
            } catch (JSONException e) {
                showToast("Invalid key");
                this.KeyInput();
            }
        } else {
            showToast("Invalid name: Special characters are not allowed");
            this.KeyInput();
        }
    }

    private void showToast(String message){
        Context context = getApplicationContext();
        CharSequence text = message;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }



    private class getMessage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            while(true){
                if(StaticValues.messageList.size() > 0) {
                    String message = StaticValues.messageList.remove(0);
                    if (message == "No")
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                StopPress(true);
                                KeyInput();
                            }
                        });
                }

                try {
                    Thread.sleep(1000, 0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }


    }

}

