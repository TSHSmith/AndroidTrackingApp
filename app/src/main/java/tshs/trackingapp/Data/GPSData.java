package tshs.trackingapp.Data;

import android.location.Location;

/**
 * Created by Thomas on 22/02/2016.
 *
 * Stores the gps lat and long in a single class, away from the location class type.
 */
public class GPSData {
    private double lati;
    private double longi;

    public GPSData(Location location){
        lati = location.getLatitude();
        longi = location.getLongitude();
    }

    public double getLati(){
        return lati;
    }

    public double getLongi(){
        return longi;
    }
}
