package tshs.trackingapp.Statics;

import java.util.ArrayList;

/**
 * Created by Thomas on 01/03/2016.
 *
 * This class is used to store the values that tell the threads to stop.
 *
 * When a Start is called for a class it is set to true and false when Stop is called.
 * This allows the threads to continue what they are doing and then exit cleanly.
 */
public class StaticValues {
    public static boolean continueAPI;
    public static ArrayList<String> messageList;
    public static boolean hasPressed = false;
}
