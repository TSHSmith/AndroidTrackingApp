package tshs.trackingapp.Threads;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

import tshs.trackingapp.MainActivity;
import tshs.trackingapp.Statics.LocalDatabase;
import tshs.trackingapp.Statics.StaticValues;

/**
 * Created by Thomas on 28/02/2016.
 *
 * Will loop through the list populated by GPSThread and send the data.
 */
public class APIThread extends Thread{

    private int numberToSend;
    private String key;
    private boolean canRun;
    private Context ctx;
    private APICom apiCom;
    /**
     * Thread loop.
     *
     * If there is an item in the queue and a connection it will attempt to send the data.
     *
     * If the APICom class doesn't return a correctly formatted json string the connection has failed,
     * the item trying to be send is left in the list and is attempted to be sent again.
     */
    @Override
    public void run(){
        while(StaticValues.continueAPI) {

            try {
                this.sleep(1000, 0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Gets all of the current data in the local database and stores it in a local cursor.
            Cursor row = LocalDatabase.rawQuery("SELECT * FROM urlTable");

            if (isNetworkAvailable() && row.getCount() > 0) {
                //gets the number of rows returned by the query.
                numberToSend = row.getCount();
                //moves the cursor to the first row.
                row.moveToFirst();
                for (int x = 0; x < numberToSend; x++) {


                    //gets a value from the apicom thread. Either a json string or
                    //raw web data. row.getstring(0) represents the url stored in the curent row of the cursor.
                    apiCom = new APICom();
                    String returnedValue = apiCom.startThread(row.getString(0));
                    try {
                        //attempts to create a new json object with the returned data.
                        JSONObject tempObj = new JSONObject(returnedValue);
                        //checks the json returned says that action was a success.
                            if (tempObj.getBoolean("success")) {
                            //removes the item from the local database.
                            //row.getInt(1) returns the unique id of the value.
                            LocalDatabase.removeRow(row.getInt(1));
                            //moves to the next item in the cursor.
                            row.moveToNext();


                        } else if (!tempObj.getBoolean("success")){
                            //Ends the thread if the value is false.
                            StaticValues.messageList.add("No");
                            StaticValues.continueAPI = false;
                            break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //if the content of returnedValue isn't a json it's not a the site we want.
                        //break from the loop and leave the Queue as it is.
                        break;
                    }
                }
            }
        }
    }

    /**
     * Starts the thread.
     * @param ctx
     * @param key
     */
    public void Start(Context ctx, String key){

        this.ctx = ctx;
        this.key = key;
        canRun = true;
        StaticValues.continueAPI = true;
        this.start();
    }

    /**
     * Stops the thread and waits for it finish. Any unsent data will be left in the list.
     */
    public void Stop(){
        StaticValues.continueAPI = false;
        try {
            this.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * checks if there is a network connection using the connectivity service.
     *
     * This function is not 100% accurate. If the device is out of data but being
     * redirected to the top up page this will still return true.
     * @return
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
