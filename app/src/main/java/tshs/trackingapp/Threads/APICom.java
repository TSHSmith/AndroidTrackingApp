package tshs.trackingapp.Threads;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class APICom extends Thread{

    //Private variables.
    private String json = "";
    private String url = "";

    /**
     * Run override.
     *
     * Calls the getJson function to retrieve the data from the website. Is called from within the
     * Start function (see "this.start()").
     */
    @Override
    public void run(){

        try {
            json = getJson(this.url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            json = e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            json = e.getMessage();
        }
    }

    /**
     * function called to start the thread takes a URL in the form of a String.
     *
     * The thread is started, but join is also called. Forcing the application to wait until the
     * data is retrieved.
     * @param url
     * @return
     */
    public String startThread(String url){
        try {
            this.url = url;
            this.start();
            this.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            json = e.getMessage();
        }
        return json;
    }

    /**
     * Takes a url param and downloads the data on the page as an inputstream.
     *
     * The input stream is converted into a string and then returned.
     * @param url
     * @return
     * @throws IOException
     */
    private String getJson(String url) throws IOException {
        String responseString = "????";

        HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
        try {
            InputStream in = new BufferedInputStream(con.getInputStream());
            responseString = readStream(in);
        } finally {
            con.disconnect();
        }
        return responseString;
    }

    /**
     * Takes an InputStream param and returns it as a String.
     * @param is
     * @return
     */
    private String readStream(InputStream is){
        try{
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while (i != -1){
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }

}