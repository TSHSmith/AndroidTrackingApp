package tshs.trackingapp.Services;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import tshs.trackingapp.Data.GPSData;
import tshs.trackingapp.MainActivity;
import tshs.trackingapp.Statics.LocalDatabase;
import tshs.trackingapp.R;

/**
 * Created by Thomas on 23/03/2016.
 */
public class ServiceManager extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private GoogleApiClient apiClient;
    private Date date;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private String key;
    private GPSData data;
    /**
     * Called when the service is first ran.
     *
     * Builds the apiClient instance and attempts to connect to the Google Services API.
     */
    @Override
    public void onCreate(){
        if(apiClient == null){
            apiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        apiClient.connect();



    }


    /**
     * Called when the service is ran.
     *
     * Gets the key value that passed via PUTEXTRA and saved locally.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.key = intent.getStringExtra("KEY");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        Intent i = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_NO_CREATE);

        Notification notification = builder.setContentTitle("Andtrack tracking app")
                .setContentText("You are being tracked.")
                .setSmallIcon(R.drawable.search_icon)
                .setContentIntent(pendingIntent)
                .getNotification();
        startForeground(1, notification);

        return START_NOT_STICKY;
    }

    /**
     * Called when the apiClient successfully connects.
     *
     * Sets up the location request and adds it to the local instance of the Google API client.
     * Here the intervals are configured for how often the location data is gathered.
     * @param bundle
     */
    @Override
    public void onConnected(Bundle bundle) {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(1500);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * When onLocationChanged is called we take the current location and the key and use them to build a URL.
     * This url is then stored in the local "urlTable" database table.
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        data = new GPSData(location);
        date = new Date();
        //gets the date as a string using the format and replaces the space with an underscore.
        String sDate = dateFormat.format(date).replace(" ", "_");
        //builds the url string
        String url = "http://andtrackproject.azurewebsites.net/API/GPS_UP?long=" + data.getLongi() + "&lat=" + data.getLati() + "&key=" + this.key + "&date=" + sDate;
        //saves the url to the local database using the static class method
        LocalDatabase.execSQL("INSERT INTO urlTable(url) " +
                "VALUES('" + url + "');");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * When the service is requested to stop this is called and the apiClient disconnects.
     */
    @Override
    public void onDestroy(){
        apiClient.disconnect();
        stopForeground(true);
    }


    //not bindable so we return null.
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
