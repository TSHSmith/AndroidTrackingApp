package tshs.trackingapp.Controllers;

import android.content.Context;

import tshs.trackingapp.Threads.APIThread;

/**
 * Created by Thomas on 28/02/2016.
 */
public class ThreadController {
    private Context ctx;
    private String key;
    private APIThread apiThread = new APIThread();


    public ThreadController(Context ctx,String key){
        this.ctx = ctx;
        this.key = key;
    }


    public void StartApi(){
        apiThread.Start(ctx, key);
    }

    public void StopApi(){
        apiThread.Stop();
    }

    public boolean ApiRunning(){
        return apiThread.isAlive();
    }



}
